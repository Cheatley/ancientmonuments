Rails.application.routes.draw do
  get 'monumentdistance/new'

  get 'scots/new'

  get 'teams/new'

  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  get 'users/new'

  root             'static_pages#home'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'monuments' => 'static_pages#monuments'
  get 'monumentsmap' => 'static_pages#monumentsmap'
  get 'monumentsgraph' => 'static_pages#monumentsgraph'
  get 'results' => 'static_pages#results'
  get 'one'     => 'static_pages#one'
  get 'two'     => 'static_pages#two'
  get 'three'     => 'static_pages#three'
  get 'four'     => 'static_pages#four'
  get 'five'     => 'static_pages#five'
  get 'six'     => 'static_pages#six'
  get 'seven'     => 'static_pages#seven'
  get 'eight'     => 'static_pages#eight'
  get 'nine'     => 'static_pages#nine'
  get 'ten'     => 'static_pages#ten'
  get 'eleven'     => 'static_pages#eleven'
  get 'twelve'     => 'static_pages#twelve'
  get 'thirteen'     => 'static_pages#thirteen'
  get 'fourteen'     => 'static_pages#fourteen'
  get 'fifteen'     => 'static_pages#fifteen'
  get 'sixteen'     => 'static_pages#sixteen'
  get 'seventeen'     => 'static_pages#seventeen'
  get 'eighteen'     => 'static_pages#eighteen'
  get 'nineteen'     => 'static_pages#nineteen'
    get 'twenty'     => 'static_pages#twenty'
  get 'twentyone'     => 'static_pages#twentyone'
    get 'twentytwo'     => 'static_pages#twentytwo'
      get 'twentythree'     => 'static_pages#twentythree'
        get 'twentyfour'     => 'static_pages#twentyfour'
          get 'twentyfive'     => 'static_pages#twentyfive'
            get 'twentysix'     => 'static_pages#twentysix'
              get 'twentyseven'     => 'static_pages#twentyseven'
                get 'twentyeight'     => 'static_pages#twentyeight'
                  get 'twentynine'     => 'static_pages#twentynine'
                    get 'thirty'     => 'static_pages#thirtyone'
                      get 'thirtyone'     => 'static_pages#thirtyone'
                       get 'thirtytwo'     => 'static_pages#thirtytwo'

  get 'signup'  => 'users#new'
  get 'login'   => 'sessions#new'
  get 'summonersearch' => 'static_pages#summonersearch'
  post 'login'  => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
   resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
