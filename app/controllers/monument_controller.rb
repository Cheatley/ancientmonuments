class MonumentController < ApplicationController
def index
  @monuments = Monument.all
  if params[:search]
    @monuments = Monument.search(params[:search]).order("created_at DESC")
  else
    @monuments = Monument.all.order("created_at DESC")
  end
end
end
