class StaticPagesController < ApplicationController

  def home
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
      populatedata
  end
  
  def populatedata
    monuments = [
      {
        description: "Anglian high cross fragment in the churchyard of St Matthew's Church, Rastrick",
        address: 'St Matthews Church Yard, Church Street, Rastrick, Brighouse, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSKHG0DWL2000&lbrefno=23376&address=Cross+Base+In%0DChurch+Yard+%0DChurch+Street%0DRastrick%0DBrighouse%0DWest+Yorkshire%0D',
        bng_x: 413830.6284,
        bng_y: 421596.2586,
        milesfromhalifax: 5.2
      },
      {
        description: "Bowl Barrow known as Beacon Hill",
        address: 'Ringstone Edge, Barkisland, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSK4KIDWL2000&lbrefno=31493&address=Beacon+Hill%0DRingstone+Edge%0DBarkisland%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 404179.9244,
        bng_y: 418559.0682,
        milesfromhalifax: 7.9
      },
      {
        description: "Brow Pit Mine Shaft, Gin Circle, spoil heap and tramway, 270 metres to south west of Catherine Slack",
        address: 'Howcans Lane, Boothtown, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSVL0WDWL2000&lbrefno=29907&address=Brow+Pit+Mine+Shaft%0DHowcans+Lane%0DBoothtown%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 408869.6409,
        bng_y: 428458.1544,
        milesfromhalifax: 1.6
      },
      {
        description: "Cairn known as Millers Grave on Midgley Moor",
        address: 'Midgley Moor, Luddendenfoot, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSVLLEDWL2000&lbrefno=31486&address=Millers+Grave%0DMidgley+Moor%0DLuddendenfoot%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 401913.9838,
        bng_y: 428368.5498,
        milesfromhalifax: 6.9
      },
            {
        description: "Cairn on Midgley Moor, 400 metres to north east of Upper Han Royd",
        address: 'Midgley Moor, Luddendenfoot, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HT6MJADWL2000&lbrefno=31523&address=Cairn%0DMidgley+Moor%0DLuddendenfoot%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 402468.5959,
        bng_y: 427194.576,
        milesfromhalifax: 7.5 
      },
              {
        description: "Cairn With An Oval Bank on Midgley Moor, 430 metres to north east of Upper Han Royd",
        address: 'Midgley Moor, Luddendenfoot, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HT6MXNDWL2000&lbrefno=31525&address=Cairn+With+An+Oval+Bank%0DMidgley+Moor%0DLuddendenfoot%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 402669.9982,
        bng_y: 427119.9972,
        milesfromhalifax: 6.8 
      },
              {
        description: "Cairnfield on Ringstone Edge Moor, 240 metres to south west of Clay House",
        address: 'Ringstone Edge, Barkisland, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HT6KZGDWL2000&lbrefno=31509&address=Cairnfield%0DRingstone+Edge%0DBarkisland%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 404805.205,
        bng_y: 418718.6124,
        milesfromhalifax: 7.9 
      },
              {
        description: "Camp at Kirklees Park",
        address: 'Castle Hill Earthworks, Kirklees Park, Wakefield Road, Brighouse, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HT6P2VDWL2000&lbrefno=253&address=Castle+Hill+Earthworks%0DKirklees+Park+%0DWakefield+Road%0DBrighouse%0DWest+Yorkshire%0D',
        bng_x: 417354.7984,
        bng_y: 421658.1408,
        milesfromhalifax: 6.4 
      },
              {
        description: "Castle Hill Motte Castle 270 metres to north east of Rosemary Hall",
        address: 'Castle Hill, Sowerby, Sowerby Bridge, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HT6OHPDWL2000&lbrefno=29952&address=Castle+Hill+Motte+Castle%0DCastle+Hill%0DSowerby%0DSowerby+Bridge%0DWest+Yorkshire%0D%0D',
        bng_x: 404001.6334,
        bng_y: 423319.995,
        milesfromhalifax: 5.2 
      },
              {
        description: "Cup marked boundary stone known as Churn Milk Joan on Crow Hill, Midgley Moor, 580 metres to north o",
        address: 'Wadsworth Moor, Hebden Bridge, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSVIE7DWL2000&lbrefno=29129&address=Churn+Milk+Joan+%0DWadsworth+Moor%0DHebden+Bridge%0DWest+Yorkshire%0D',
        bng_x: 401977.7097,
        bng_y: 427685.4558,
        milesfromhalifax: 7.9 
      },
              {
        description: "Enclosed Bronze Age urnfield 160 metes to west of Overgreen Royd Farm",
        address: 'Overgreen Royd, Mount Tabor, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HT6ETUDWL2000&lbrefno=31482&address=Enclosed+Bronze+Age+Urnfield%0DOvergreen+Royd%0DMount+Tabor%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 405196.9856,
        bng_y: 427928.3358,
        milesfromhalifax: 4.2 
        
      },     
      
      {
        description: "Enclosed Bronze Age urnfield 200 metres to north west of Hanging Field Farm",
        address: 'Hey Head Lane, Todmorden, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HT6KQIDWL2000&lbrefno=31492&address=Enclosed+Bronze+Age+Urnfield%0DHey+Head+Lane%0DTodmorden%0D%0D',
        bng_x: 394326.5652,
        bng_y: 425425.8024,
        milesfromhalifax: 13.4 
      },     
      
      {
        description: "Enclosed Bronze Age urnfield 440 metres to north west of Rough Bottom on Midgley Moor",
        address: 'Midgley Moor, Luddendenfoot, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSVM43DWL2000&lbrefno=31483&address=Bronze+Age+Urnfield%0DMidgley+Moor%0DLuddendenfoot%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 401334.0345,
        bng_y: 427388.3706,
        milesfromhalifax: 7.8 
      },
      
      {
        description: "Late Prehistoric Enclosed Settlement 350 metres to south west of Goose Clough",
        address: 'Ovenden Moor, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HT6FSPDWL2000&lbrefno=31484&address=Late+Pre-historic+Enclosed+Settlement%0DOvenden+Moor%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 405521.8412,
        bng_y: 429385.4292,
        milesfromhalifax: 3.6 
      },
      
       {
        description: "Late prehistoric enclosed settlement 500 metres to north west of Goose Clough on Ovenden Moor",
        address: 'Ovenden Moor, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HT6FAMDWL2000&lbrefno=31485&address=Late+Prehistoric+Enclosed+Settlement%0DOvenden+Moor%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 405547.9201,
        bng_y: 429938.6406,
        milesfromhalifax: 5.5 
       },
      
       {
        description: "Magna Via",
        address: 'Dark Lane, Southowram, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSKG1SDWL2000&lbrefno=1280&address=Magna+Via%0DDark+Lane%0DSouthowram%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 411359.3939,
        bng_y: 425261.3196,
        milesfromhalifax: 2.3 
 },
      
       {
        description: "Meg Dike late prehistoric enclosed settlement",
        address: 'Scammonden Road, Barkisland, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSK4WUDWL2000&lbrefno=31526&address=Meg+Dike%0DScammonden+Road%0DBarkisland%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 404984.5846,
        bng_y: 417473.3886,
        milesfromhalifax: 8.8 
       },
      
       {
        description: "Mill Gas Plant, Shaw Lodge Mills",
        address: 'Shaw Lodge Mills, Boys Lane, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSKJPLDWL2000&lbrefno=1288&address=Mill+Gas+Plant+Shaw+Lodge+Mills%0DBoyes+Lane%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 409688.664,
        bng_y: 424047.1872,
        milesfromhalifax: 1.8 
      
      },
      
       {
        description: "Old Church of St Thomas Becket, 210 metres to north east of Daisy Field Farm",
        address: 'Church Street, Heptonstall, Hebden Bridge, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSK3HVDWL2000&lbrefno=29954&address=Church+Of+St+Thomas+A+Becket+%0DChurch+Street%0DHeptonstall%0DHebden+Bridge%0DWest+Yorkshire%0D',
        bng_x: 398674.219,
        bng_y: 428057.9988,
        milesfromhalifax: 10.3 
      },   
      
       {
        description: "Oxygrains Packhorse Bridge, Rishworth",
        address: 'Oldham Road, Rishworth, Sowerby Bridge, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSK5NQDWL2000&lbrefno=142&address=Oxygrains+Packhorse+Bridge%0DOldham+Road%0DRishworth%0DSowerby+Bridge%0DWest+Yorkshire%0D%0D',
        bng_x: 400412.0799,
        bng_y: 415841.7558,
        milesfromhalifax: 10.9 
      },   
      
       {
        description: "Ring Cairn known as the Ring of Stones on Ringstone Edge Moor, 800 metres to south of Upper Gosling",
        address: 'Ringstone Edge, Barkisland, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSK6LNDWL2000&lbrefno=31508&address=Ring+Cairn%0DRingstone+Edge%0DBarkisland%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 404438.4236,
        bng_y: 418251.3258,
        milesfromhalifax: 8.1 
      },
      
       {
        description: "Ring Cairn on Midgley Moor, 360 metres to north east of Upper Han Royd",
        address: 'Midgley Moor, Luddendenfoot, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HT6MPMDWL2000&lbrefno=31524&address=Ring+Cairn%0DMidgley+Moor%0DLuddendenfoot%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 402619.7032,
        bng_y: 427041.0798,
        milesfromhalifax: 7.3 
      },
      
      {
        description: "Ripponden (or Waterloo) Bridge",
        address: 'Bridge End, Ripponden, Sowerby Bridge, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSK5U5DWL2000&lbrefno=215&address=Ripponden+Old+Bridge+%0DBridge+End%0DRipponden%0DSowerby+Bridge%0DWest+Yorkshire%0D',
        bng_x: 404065.2686,
        bng_y: 419782.1178,
        milesfromhalifax: 6.8 
      },

      {
        description: "Roman Road, Blackstone Edge, Ripponden",
        address: 'Blackstone Edge, Rochdale Road,Ripponden, Sowerby Bridge, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSKFAKDWL2000&lbrefno=32&address=Roman+Road%2C+Blackstone+Edge%0DRochdale+Road%0DRipponden%0DSowerby+Bridge%0DWest+Yorkshire%0D%0D',
        bng_x: 397792.0881,
        bng_y: 417422.8632,
        milesfromhalifax: 13.6 
      },

      {
        description: "The Gibbet Platform, Halifax",
        address: 'Bedford Street North, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSKFD7DWL2000&lbrefno=131&address=Remains+Of+Gibbet+%0DBedford+Street+North%0DHalifax%0DWest+Yorkshire%0D',
        bng_x: 408848.3686,
        bng_y: 425261.979,
        milesfromhalifax: 1.0 
      },

      {
        description: "The Old Bridge over the Hebden Water, Hebden Bridge",
        address: 'Old Gate, Hebden Bridge, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSK3MUDWL2000&lbrefno=53&address=The+Old+Bridge+%0DOld+Gate%0DHebden+Bridge%0DWest+Yorkshire%0D',
        bng_x: 399216.7771,
        bng_y: 427295.0574,
        milesfromhalifax: 8.5 
      },

      {
        description: "Ventilation Chimney and Furnace House 260 metres to south of Park Farm",
        address: 'Shibden Hall Road, Halifax, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HT6GLSDWL2000&lbrefno=30960&address=Ventilation+Chimney+And+Furnace+House%0DShibden+Hall+Road%0DHalifax%0DWest+Yorkshire%0D%0D',
        bng_x: 410343.1654,
        bng_y: 425515.497,
        milesfromhalifax: 1.1 
      },

      {
        description: "Wayside Cross known as Abel Cross",
        address: 'Abel Cote Wood Lane, Wadsworth, Hebden Bridge, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSK499DWL2000&lbrefno=23360&address=Abel+Cross%0DAbel+Cote+Wood+Lane%0DWadsworth%0DHebden+Bridge%0DWest+Yorkshire%0D%0D',
        bng_x: 398622.5471,
        bng_y: 430703.6214,
        milesfromhalifax: 11.4 
      },

      {
        description: "Wayside Cross known as Mount Cross",
        address: 'Kebs Road, Todmorden, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSK54ODWL2000&lbrefno=23359&address=Mount+Cross%0DKebs+Road%0DTodmorden%0D%0D',
        bng_x: 391463.0804,
        bng_y: 427283.151,
        milesfromhalifax: 14.4 
      },
      
      {
        description: "Wayside cross known as Reaps Cross",
        address: 'Reaps Edge, Heptonstall Moor, Heptonstall, Hebden Bridge, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSLZ32DWL2000&lbrefno=23383&address=Reaps+Cross+%0DReaps+Edge%2C+Heptonstall+Moor%0DHeptonstall%0DHebden+Bridge%0DWest+Yorkshire%0D',
        bng_x: 394358.391,
        bng_y: 430267.4964,
        milesfromhalifax: 13.8 
      },
      
      {
        description: "Wayside Cross known as Tinker Cross",
        address: 'Lee Wood Road, Heptonstall, Hebden Bridge, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSVI5RDWL2000&lbrefno=23382&address=Tinker+Cross%0DLee+Wood+Road%0DHeptonstall%0DHebden+Bridge%0DWest+Yorkshire%0D%0D',
        bng_x: 398913.4492,
        bng_y: 428175.0954,
        milesfromhalifax: 10.3 
      },
      
      {
        description: "Wayside Cross located on Slate Pit Hill",
        address: 'Slate Pit Hill, Rochdale Road, Ripponden, Sowerby Bridge, West Yorkshire',
        information: 'http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSKK38DWL2000&lbrefno=23384&address=Cross+At+Slate+Pit+Hill%0DRochdale+Road%0DRipponden%0DSowerby+Bridge%0DWest+Yorkshire%0D%0D',
        bng_x: 398553.5278,
        bng_y: 418065.1092,
        milesfromhalifax: 10.6 
      }
      
      ]
    Monument.delete_all
    monuments.each do |monument|
      insertion = Monument.new(description: monument[:description], address: monument[:address], information: monument[:information], bng_x: monument[:bng_x], bng_y: monument[:bng_y], milesfromhalifax: monument[:milesfromhalifax])
      insertion.save
    end
    user = User.find_by(name: 'mario')
    if user == nil
      user = User.new(name: 'mario', email: 'goombas@nintendo.com', password: 'whatever', password_confirmation: 'whatever', admin: true, activated: true)
      user.save
    else
      user.update(name: 'mario', email: 'goombas@nintendo.com', password: 'whatever', password_confirmation: 'whatever', admin: true, activated: true)
    end
  end
  
#Still a problem, should I check the logs again?
#no need to migrate. yeah
  def help
  end
  
  def statspage
  end

  def about
  end

  def contact
  end
  
  def monuments
  end
  
  def monumentsmap
  end
  
  def monumentsgraph
   @monuments = Monument.all

  end
  
  def results
    @monuments = Monument.all
  end
  
  def one
    anglianhighcross = Monument.all.order(id: :asc)[0]
    @description = anglianhighcross.description
    @address = anglianhighcross.address
    @distance = anglianhighcross.milesfromhalifax
  end
  
  def two
    two = Monument.all.order(id: :asc)[1]
    @description = two.description
    @address = two.address
    @distance = two.milesfromhalifax
  end
  
  def three
    three = Monument.all.order(id: :asc)[2]
    @description = three.description
    @address = three.address
    @distance = three.milesfromhalifax
  end
  
  def four
    four = Monument.all.order(id: :asc)[3]
    @description = four.description
    @address = four.address
    @distance = four.milesfromhalifax
  end
  
  def five
    five = Monument.all.order(id: :asc)[4]
    @description = five.description
    @address = five.address
    @distance = five.milesfromhalifax
  end
  
  def six
    six = Monument.all.order(id: :asc)[5]
    @description = six.description
    @address = six.address
    @distance = six.milesfromhalifax
  end 
  
  def seven
    seven = Monument.all.order(id: :asc)[6]
    @description = seven.description
    @address = seven.address
    @distance = seven.milesfromhalifax
  end
  
  def eight
    eight = Monument.all.order(id: :asc)[7]
    @description = eight.description
    @address = eight.address
    @distance = eight.milesfromhalifax
  end
  
  def nine
    nine = Monument.all.order(id: :asc)[8]
    @description = nine.description
    @address = nine.address
    @distance = nine.milesfromhalifax
  end
  
  def ten
    ten = Monument.all.order(id: :asc)[9]
    @description = ten.description
    @address = ten.address
    @distance = ten.milesfromhalifax
  end
  
  def eleven
    eleven = Monument.all.order(id: :asc)[10]
    @description = eleven.description
    @address = eleven.address
    @distance = eleven.milesfromhalifax
  end
  
  def twelve
    twelve = Monument.all.order(id: :asc)[11]
    @description = twelve.description
    @address = twelve.address
    @distance = twelve.milesfromhalifax
  end 
  
  def thirteen
    thirteen = Monument.all.order(id: :asc)[12]
    @description = thirteen.description
    @address = thirteen.address
    @distance = thirteen.milesfromhalifax
  end
    
  def fourteen
    fourteen = Monument.all.order(id: :asc)[13]
    @description = fourteen.description
    @address = fourteen.address
    @distance = fourteen.milesfromhalifax
  end
    
  def fifteen
    fifteen = Monument.all.order(id: :asc)[14]
    @description = fifteen.description
    @address = fifteen.address
    @distance = fifteen.milesfromhalifax
  end
    
  def sixteen
    sixteen = Monument.all.order(id: :asc)[15]
    @description = sixteen.description
    @address = sixteen.address
    @distance = sixteen.milesfromhalifax
  end
    
  def seventeen
    seventeen = Monument.all.order(id: :asc)[16]
    @description = seventeen.description
    @address = seventeen.address
    @distance = seventeen.milesfromhalifax
  end
    
  def eighteen
    eighteen = Monument.all.order(id: :asc)[17]
    @description = eighteen.description
    @address = eighteen.address
    @distance = eighteen.milesfromhalifax
  end
    
  def nineteen
    nineteen = Monument.all.order(id: :asc)[18]
    @description = nineteen.description
    @address = nineteen.address
    @distance = nineteen.milesfromhalifax
  end
    
  def twenty
    twenty = Monument.all.order(id: :asc)[19]
    @description = twenty.description
    @address = twenty.address
    @distance = twenty.milesfromhalifax
  end
  
  def twentyone
    twentyone = Monument.all.order(id: :asc)[20]
    @description = twentyone.description
    @address = twentyone.address
    @distance = twentyone.milesfromhalifax
  end
  
  def twentytwo
    twentytwo = Monument.all.order(id: :asc)[21]
    @description = twentytwo.description
    @address = twentytwo.address
    @distance = twentytwo.milesfromhalifax
  end
  
  def twentythree
    twentythree = Monument.all.order(id: :asc)[22]
    @description = twentythree.description
    @address = twentythree.address
    @distance = twentythree.milesfromhalifax
  end
  
  def twentyfour
    twentyfour = Monument.all.order(id: :asc)[23]
    @description = twentyfour.description
    @address = twentyfour.address
    @distance = twentyfour.milesfromhalifax
  end
  
  def twentyfive
    twentyfive = Monument.all.order(id: :asc)[24]
    @description = twentyfive.description
    @address = twentyfive.address
    @distance = twentyfive.milesfromhalifax
  end
  
  def twentysix
    twentysix = Monument.all.order(id: :asc)[25]
    @description = twentysix.description
    @address = twentysix.address
    @distance = twentysix.milesfromhalifax
  end
  
  def twentyseven
    twentyseven = Monument.all.order(id: :asc)[26]
    @description = twentyseven.description
    @address = twentyseven.address
    @distance = twentyseven.milesfromhalifax
  end
  
    def twentyeight
    twentyeight = Monument.all.order(id: :asc)[27]
    @description = twentyeight.description
    @address = twentyeight.address
    @distance = twentyeight.milesfromhalifax
    end
  
  def twentynine
    twentynine = Monument.all.order(id: :asc)[28]
    @description = twentynine.description
    @address = twentynine.address
    @distance = twentynine.milesfromhalifax
  end
  
  def thirty
    thirty = Monument.all.order(id: :asc)[29]
    @description = thirty.description
    @address = thirty.address
    @distance = thirty.milesfromhalifax
  end
  
  def thirtyone
    thirtyone = Monument.all.order(id: :asc)[30]
    @description = thirtyone.description
    @address = thirtyone.address
    @distance = thirtyone.milesfromhalifax
  end
  
  def thirtytwo
    thirtytwo = Monument.all.order(id: :asc)[31]
    @description = thirtytwo.description
    @address = thirtytwo.address
    @distance = thirtytwo.milesfromhalifax
  end
  def summonersearch
    hit = Monument.where("description LIKE ?", "%#{params[:search]}%").first
    hit = Monument.where("address LIKE ?", "%#{params[:search]}%").first
    hit = Monument.where("milesfromhalifax LIKE ?", "%#{params[:search]}%").first
    if hit != nil
      @description = hit.description
      @address = hit.address
      @distance = hit.milesfromhalifax
    else
      redirect_to '/zomg no results page'
    end
  end
  
end