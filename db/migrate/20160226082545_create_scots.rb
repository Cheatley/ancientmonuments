class CreateScots < ActiveRecord::Migration
  def change
    create_table :scots do |t|
      t.string :team
      t.integer :wins
      t.integer :losses

      t.timestamps null: false
    end
  end
end
