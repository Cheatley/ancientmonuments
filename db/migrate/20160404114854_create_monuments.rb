class CreateMonuments < ActiveRecord::Migration
  def change
    create_table :monuments do |t|
      t.string :description
      t.string :address
      t.string :information
      t.decimal :bng_x
      t.decimal :bng_y

      t.timestamps null: false
    end
  end
end
