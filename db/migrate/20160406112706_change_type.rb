class ChangeType < ActiveRecord::Migration
  def change
     change_column :monuments, :milesfromhalifax, :decimal
  end
end
