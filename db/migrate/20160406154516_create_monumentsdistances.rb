class CreateMonumentsdistances < ActiveRecord::Migration
  def change
    create_table :monumentsdistances do |t|
      t.decimal :milesfromhalifax

      t.timestamps null: false
    end
  end
end
