class ChangeNames < ActiveRecord::Migration
  def change
    rename_column :monuments, :distancefromhalifax, :milesfromhalifax
  end
end
