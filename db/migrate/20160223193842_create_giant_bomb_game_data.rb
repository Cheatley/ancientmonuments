class CreateGiantBombGameData < ActiveRecord::Migration
  def change
    create_table :giant_bomb_game_data do |t|
      t.string :name
      t.string :releasedate

      t.timestamps null: false
    end
  end
end
